#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  objects_property_list.py
#  

"""
Macro to list the properties of all objects within a FreeCAD model.

Output is sent to the "Report view" which may need to be enabled using
View > Panels > Report view

02/10/2018
Matt Hawkins
https://www.tech-spy.co.uk/category/development/freecad/

Official FreeCAD site : https://www.freecadweb.org/

"""

import FreeCAD
from PySide import QtGui

# Clear Report view
mwin=Gui.getMainWindow()
r=mwin.findChild(QtGui.QTextEdit, "Report view")
r.clear()

App.Console.PrintMessage("============================\n")
App.Console.PrintMessage("  objects_property_list     \n")
App.Console.PrintMessage("============================\n\n")

# Get all objects
doc = FreeCAD.ActiveDocument
objs = FreeCAD.ActiveDocument.Objects

for obj in objs:
  # Get object properties
  a = obj.Name
  b = obj.Label
  p = obj.PropertiesList
  ti = obj.TypeId

  App.Console.PrintMessage("Part : " + str(b) + " " + str(ti) + "\n")

  for propertyname in p:
    propertyvalue = obj.getPropertyByName (propertyname)
    App.Console.PrintMessage(" " + str(propertyname) + " : " + str(propertyvalue) + "\n")

  App.Console.PrintMessage("\n")
