#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  parts_dimensions_list.py
#

"""
Macro to list the dimensions of objects within a FreeCAD model.

I created this to print out a parts list with dimensions for
cutting up lengths of wood. As such it is fairly basic and
suited for models made with basic shapes such as boxes and cylinders.

Output is sent to the "Report view" which may need to be enabled using
View > Panels > Report view

02/10/2018
Matt Hawkins
https://www.tech-spy.co.uk/category/development/freecad/

Official FreeCAD site : https://www.freecadweb.org/

"""

import FreeCAD
from PySide import QtGui

# Clear Report view
mwin=Gui.getMainWindow()
r=mwin.findChild(QtGui.QTextEdit, "Report view")
r.clear()

App.Console.PrintMessage("============================\n")
App.Console.PrintMessage("  parts_dimensions_list     \n")
App.Console.PrintMessage("============================\n\n")

# Get all objects
doc = FreeCAD.ActiveDocument
objs = FreeCAD.ActiveDocument.Objects

oc = 0

for obj in objs:
	# Get object properties
	a = obj.Name
	b = obj.Label
	ti = obj.TypeId
	shape = ti[ti.find('::')+2:]

	# Get dimensions for cylinders and boxes
	if ti=='Part::Cylinder':
		r = obj.getPropertyByName ("Radius")
		h = obj.getPropertyByName ("Height")
		l=2*r
		w=2*r
	elif ti=='Part::Box':
		r=0
		h = obj.getPropertyByName ("Height")
		l = obj.getPropertyByName ("Length")
		w = obj.getPropertyByName ("Width")
	else:
		r=0
		h=0
		l=0
		w=0

	if h<>0:
		# Objects we are interested in have a height
		App.Console.PrintMessage(str(b) + "\n")

		oc=oc+1

		# Output values to the Report panel
		if r!=0:
			App.Console.PrintMessage("  Radius : " + str(r) + "\n")

		App.Console.PrintMessage("  Height : " + str(h) + "\n")
		App.Console.PrintMessage("  Length : " + str(l) + "\n")
		App.Console.PrintMessage("  Width : " + str(w) + "\n")

		App.Console.PrintMessage("\n")

App.Console.PrintMessage("Total Part Count : " + str(oc) + "\n")
