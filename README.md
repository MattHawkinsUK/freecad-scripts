# FreeCAD Scripts

Collection of Python scripts that can be used to perform tasks in FreeCAD, the open-source parametric 3D modeler.

##clear_report_view.py
FreeCAD macro to clear the Report view panel.

##objects_property_list
FreeCAD macro to list the properties of all objects in the "Report view" panel.

##parts_dimensions_list
FreeCAD macro to list the dimensions of objects in the "Report view" panel.
